import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./pages/Login";
import CreateLogin from "./pages/Login/Create";
import Dashboard from "./pages/Dashboard";
import DashboardCoordenacao from "./pages/DashboardCoordenacao";
import Table from "./pages/Dashboard/Table";
import TableHistoric from "./pages/Dashboard/TableHistoric";
import Form from "./pages/Form";
import FormSuccess from "./pages/Form/Success";
import TemplateBase from "./pages/TemplateBase";

function AppRoutes() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<TemplateBase/>}> 
                    <Route path="/login" element={<Login/>} />    
                    <Route path="/create-login" element={<CreateLogin/>} />  

                    <Route path="/create-order" element={<Form/>} />  
                    <Route path="/view-order" element={<Table/>} />  
                    <Route path="/success-submit" element={<FormSuccess/>} />  
                    <Route path="/historic" element={<TableHistoric/>} />  

                    <Route path="/dashboard" element={<Dashboard/>} />    
                    <Route path="/dashboard" element={<Dashboard/>} />

                    <Route path="/dashboard-coordenacao" element={<DashboardCoordenacao/>} />    
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default AppRoutes