import './App.css';
import AppRoutes from './AppRoutes';
import './pages/Login';


function App() {
  return (
    <>
      <AppRoutes />
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM"
      />
    </>
  );
}

export default App;
