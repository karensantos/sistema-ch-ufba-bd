import './asstes/style.css';

export default function DashboardCoordenacao() {
    return(
        <>
            <div>
                <br/>
                <h1>Área da coordenação</h1>
            </div>
            <div className="wrapper-cards-acitons">
                <a className="card-action" href="/">
                    <i className="fa-sharp fa-solid fa-file-invoice"></i>
                    <p>Solicitações em andamento</p>
                </a>

                <a className="card-action" href="/">
                    <i className="fa-solid fa-arrows-rotate"></i>
                    <p>Solicitações em recurso</p>
                </a>

                <a className="card-action" href="/">
                    <i className="fa-solid fa-clock-rotate-left"></i>
                    <p>Histórico de solicitações encerradas</p>
                </a>
            </div>
        </>
    )
}