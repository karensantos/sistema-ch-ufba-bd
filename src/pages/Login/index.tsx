import '../../style/login.css'
import { useState } from "react"

export default function Login() {

    const [value, setValue] = useState('react');

    return(
        <>                                                  
        <div className="container">
                <div className="form-container" id="login-form">
                    <h1>Login</h1>    
                    <p>Entre com os dados da sua conta de e-mail @ufba.br</p>                                                         
                    <br/>
                    <form>
                        <label htmlFor="cargo">Eu sou </label>
                        <select 
                        onChange={event => setValue(event.target.value)}
                        defaultValue={value}
                        className="select form-control" id="dropdown">
                            <option value="aluno">Aluno</option>
                            <option value="coordenacao">Coordenacao</option>
                        </select>
                        <br/>

                        <label htmlFor="useremail">E-mail</label>
                        <input type="text" id="useremail" name="useremail" required/>

                        <label htmlFor="password">Senha</label>
                        <input type="password" id="password" name="password" required/>
                        <a className="btn-login" href={value == "coordenacao" ? '/dashboard-coordenacao':'/dashboard'} type="submit">Entrar</a >
                    </form>
                    <p>Esqueceu a senha ?<a href="https://autenticacao.ufba.br/u/redefinir-senha" id="signup-link"> Você será redirecionado ao site STI</a></p>
                </div>  
            </div>
        </>
    )
}   