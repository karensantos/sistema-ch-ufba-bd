import './style/style.css';

export default function CreateLogin() {
    return(
        <>                                                  
        <div className="container">
            <div className="form-container" id="login-form">
                <h1>Criar conta</h1>                                                              
                <form >
                    <label htmlFor="new-user-full-name">Nome completo</label>
                    <input type="text" id="new-user-full-name" name="new-user-full-name" required/>

                    <label htmlFor="new-user-course">Curso</label>
                    <input type="text" id="new-user-course" name="new-user-course" required/>

                    <label htmlFor="new-user-registration">Número de matrícula</label>
                    <input type="text" id="new-user-registration" name="new-user-registration" required/>

                    <label htmlFor="new-user-email">Email (@ufba)</label>
                    <input type="email" id="new-user-email" name="new-user-email" required/>

                    <label htmlFor="new-user-phone">Telefone</label>
                    <input type="email" id="new-user-phone" name="new-user-phone" required/>

                    <label htmlFor="new-user-password">Senha</label>
                    <input type="password" id="new-user-password" name="new-user-password" required/>
                    <button type="submit">Criar</button>                
                </form>
            </div>
        </div>
        </>
    )
}