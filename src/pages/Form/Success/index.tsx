import '../style/style.css'

export default function FormSuccess() {
    return(
        <>
        <div className="container">
            <div className="row">    
                  <div id="form-tagline" className="col-md-4">
                    <div className="form-tagline">
                        <i className="fa-solid fa-clipboard-check fa-5x"></i>
                        <h2>Submissão de documentos de aproveitamento de CH</h2>
                    </div>
                </div>
         
              <div id="form-content" className="col-md-8">
                <h1>Solicitação enviada com sucesso</h1>
                <div className="text-center">
                    <p>Pedido número: #0002023</p>
                    <p>Fique atento e acompanhe o processo atraves do número acima no seu painel</p>
                    <br/>
                    <a  href="/dashboard" id="submit"
                  type="submit"  className="btn btn-default" aria-pressed="true">Voltar para Dashboard</a>

                </div>
              </div>
              </div>

        </div>
        </>
    )
}