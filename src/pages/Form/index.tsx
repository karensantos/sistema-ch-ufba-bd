import './style/style.css'

export default function Form() {
    return(
        <>
        <div className="container">
            <div className="row">    
                  <div id="form-tagline" className="col-md-4">
                    <div className="form-tagline">
                        <i className="fa fa-envelope fa-5x"></i>
                        <h2>Submissão de documentos de aproveitamento de CH</h2>
                        <p id="description" className="lead">Atenção ao preenchimento dos campos</p>
                    </div>
                </div>
         
		        <div id="form-content" className="col-md-8">
            
                <form id="survey-form">                              
                    <div className="row form-group">
                        <h3>Informaçoes do aluno</h3>
                        <div className="col-sm-3">
                            <label id="name-label" className="control-label" htmlFor="name">*Name:</label>
                        </div>
                        
                        <div className="input-group col-sm-9">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="basic-addon-name"><i className="fa fa-user"></i></span>
                            </div>                    
                            <input id="name" type="text" className="form-control" placeholder="Seu nome completo" name="name" required/>
                        </div>
                    </div>
                 
                <div className="form-group row">
                   <div className="col-sm-3">
                     <label id="email-label" className="control-label" htmlFor="email">*Email:</label>
                   </div>
                 
                   <div className="input-group col-sm-9">
                     <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon-mail"><i className="fa fa-envelope"></i></span>
                     </div>
                     <input type="email" className="form-control" id="email" placeholder="Seu e-mail @ufba.br" name="email" pattern="^[-+.\w]{1,64}@[-.\w]{1,64}\.[-.\w]{2,6}$" required/>
                   </div>
                </div>

                <div className="form-group row">
                   <div className="col-sm-3">
                     <label id="email-label" className="control-label" htmlFor="email">*Curso:</label>
                   </div>
                 
                   <div className="input-group col-sm-9">
                     <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon-mail"><i className="fa-solid fa-graduation-cap"></i></span>
                     </div>
                     <input type="text" className="form-control" id="email" placeholder="Seu curso" name="email" required/>
                   </div>
                </div>
                 
                <div className="form-group row">
                   <div className="col-sm-3">
                     <label id="matricula-label" className="control-label" htmlFor="matricula">*Matrícula:</label>
                   </div>
                 
                   <div className="input-group col-sm-9">
                     <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon-mail"><i className="fa-solid fa-arrow-down-9-1"></i></span>
                     </div>
                     <input type="text" className="form-control" idfa-duotone fa-address-card="matricula" placeholder="Sua matrícula" name="matricula" required/>
                   </div>
                </div>
                <h2>Submissão de documentos de aproveitamento de CH</h2>
                <div className="form-group row">
                   <div className="col-sm-3">
                     <label id="matricula-label" className="control-label" htmlFor="telefone">*Telefone:</label>
                   </div>
                 
                   <div className="input-group col-sm-9">
                     <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon-mail"><i className="fa-solid fa-phone"></i></span>
                     </div>
                     <input type="text" className="form-control" idfa-duotone fa-address-card="telefone" placeholder="Sueu telefone para contato" name="telefone" required/>
                   </div>
                </div>
                <hr/>
                 
                <div className="form-group row">    
                   <div className="col-sm-3">
                     <label className="control-label"htmlFor="visit-purpose">Tópico de ajuda:</label>
                   </div>
                   
                   <div className="input-group col-sm-9">
                     <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon-purpose"><i className="fa fa-hotel"></i></span>
                     </div>
                     
                     <select className="form-control" id="dropdown">
                       <option>Selecione um tópico de ajuda</option>
                       <option>Graduação / Aproveitar CH complementar</option>
                     </select>
                     
                   </div>     
                </div>
                 

           
              <hr/>  

              <div className="row form-group">
                  <div className="col-sm-12">
                      <label  className="control-label" >Aproveitamento de carga horária complementar</label>
                      <div className="list">
                        <ul>
                          <li>Orientações para alunos do BCC:  <a href="hhttps://bcc.ufba.br/atividades-complementares">https://bcc.ufba.br/atividades-complementares</a></li>

                          <li>Orientações para alunos do BSI:  <a href="hhttps://bsi.ufba.br/atividades-complementares">https://bsi.ufba.br/atividades-complementares</a></li>

                          <li>Orientações para alunos da LC: <a href="https://drive.google.com/file/d/1m2opI3Zmo6Q1In-whek-tiImpglqP09D/view">https://drive.google.com/file/d/1m2opI3Zmo6Q1In-whek-tiImpglqP09D/view</a></li> 
                        </ul>
                      </div>
                  </div>
              </div>

              <div className="row form-group">
                  <div className="col-sm-12">
                   <label className="control-label" htmlFor="description-order">Lista de atividades a serem aproveitadas *:</label>
                 </div>
                  
                 <div className="input-group col-sm-9">
                     <div className="description-order">
                        <span className="input-group-text" id="basic-addon-mail"><i className="fa fa-comment"></i></span>
                     </div>
                  <textarea className="form-control" id="description-order"></textarea>
                 </div>
              </div>

              <div className="row form-group">
                  <div className="col-sm-12">
                   <label className="control-label" htmlFor="description-order">Comprovantes (PDF) *:</label>
                 </div>
                  
                 <div className="input-group col-sm-9">
                     <input type='file' required/> 
                 </div>
              </div>
              
              <hr/>
                 
              <div className="form-group row"> 
                 <div className="col-sm-3">
                   <label className="control-label" htmlFor="comment">Observações:</label>
                 </div>
                  
                 <div className="input-group col-sm-9">
                     <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon-mail"><i className="fa fa-comment"></i></span>
                     </div>
                  <textarea className="form-control" id="comment"></textarea>
                 </div>  
              </div>
                
              <div className="form-group row">
                <div className="col-sm-12 submit-button">
                  <a  href="/success-submit"
                  type="submit" id="submit" className="btn btn-default" aria-pressed="true">Enviar para análise</a>
                </div>
              </div>
                               
            </form> 

		    </div> 
               
            </div> 
        </div>
        </>
    )
}