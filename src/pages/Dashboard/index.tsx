import './asstes/style.css';

export default function Dashboard() {
    return(
        <>
            <div>
                <br/>
                <h1>Área do aluno</h1>
            </div>

            <div className="wrapper-cards-acitons">
                <a className="card-action" href="/create-order">
                    <i className="fa-solid fa-plus"></i>
                    <p>Criar solicitação</p>
                </a>

                <a className="card-action" href="/view-order">
                    <i className="fa-sharp fa-solid fa-file-invoice"></i>
                    <p>Acompanhar solicitações em andamento</p>
                </a>

                <a className="card-action" href="/historic">
                    <i className="fa-solid fa-clock-rotate-left"></i>
                    <p>Ver histórico de solicitações</p>
                </a>
            </div>
        </>
    )
}