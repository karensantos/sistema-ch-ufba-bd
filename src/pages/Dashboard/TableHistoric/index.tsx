import '../asstes/style.css';
import '../asstes/table.css';

export default function TableHistoric() {
    return (
        <>
            <div className="hader-dashboard text-center">
                <br />
                <h1>Área do aluno</h1>
                <h3>Histórico de solicitações</h3>
            </div>

            <div className="wrapper-table">
            <div className="row">
                <div className="col-12">
                    <div className="wrapper-cards-acitons">
                        <table>
                            <thead>
                                <tr>
                                    <th>Nº Pedido</th>
                                    <th>Aberto em</th>
                                    <th>Descrição</th>
                                    <th>CH total</th>
                                    <th className="text-right"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-label="order">#4554333</td>
                                    <td data-label="Curso">11/07/2023</td>
                                    <td data-label="Curso">Curso de JavaScript - online</td>
                                    <td data-label="Duração">13h</td>
                                    <td>
                                        <button className="ap status">Aprovado</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="order">#333133</td>
                                    <td data-label="Curso">11/07/2023</td>
                                    <td data-label="Curso">Curso de formação em HTML e CSS - online</td>
                                    <td data-label="Duração">10h</td>
                                    <td>
                                        <button className="neg status">Negado</button>
                                        <button className="rec">Solicitar recurso</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="order">#00033</td>
                                    <td data-label="Curso">11/07/2023</td>
                                    <td data-label="Curso">Palestra sobre Impactos da IA nos tempos atuais</td>
                                    <td data-label="Duração">40h</td>
                                    <td>
                                    <button className="ap status">Aprovado</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="order">#45000</td>
                                    <td data-label="Curso">11/07/2023</td>
                                    <td data-label="Curso">Introdução a lógica de programação</td>
                                    <td data-label="Duração">40h</td>
                                    <td>
                                        <button className="neg status">Negado</button>
                                        <button className="rec">Solicitar recurso</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="order">#9098800</td>
                                    <td data-label="Curso">11/07/2023</td>
                                    <td data-label="Curso">Python para iniciantes</td>
                                    <td data-label="Duração">5h</td>
                                    <td>
                                    <button className="ap status">Aprovado</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td data-label="order">#102222</td>
                                    <td data-label="Curso">11/07/2023</td>
                                    <td data-label="Curso">Monitoria em Computador Ética e Sociedade</td>
                                    <td data-label="Duração">70h</td>
                                    <td>
                                    <button className="ap status">Aprovado</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </>
    )
}