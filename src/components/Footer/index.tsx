import './style/style.css'

function Footer() {
    return (
        <>
        <div className="footer">
            <p>2023 © Desenvolvido para disciplina BD - MATA60 | Projeto fictício sem fins comerciais.</p>
        </div>
        </>
    )
}

export default Footer;